"""
This file is to transform cifar10 raw data to pytorch ImageFolder structure, 
it's like something following:
/path/to/cifar10
    ├── train
    │       │   ├── airplane
    │       │   │   ├── aeroplane_s_000002.png
    │       │   │   ├── aeroplane_s_000040.png
    │       │   │   ├── aeroplane_s_000045.png
    │       │   │   ├── aeroplane_s_000063.png
    ├── test
    │       │   ├── airplane
    │       │   │   ├── aeroplane_s_000002.png
    │       │   │   ├── aeroplane_s_000040.png
    │       │   │   ├── aeroplane_s_000045.png
    │       │   │   ├── aeroplane_s_000063.png
"""
import os, sys
import pickle

import numpy as np
from PIL import Image
from tqdm import tqdm


def main():
    root = "./"
    make_for_cifar10(root)

def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict


def generate_class_dir(train, labels):
    if train:
        mode = "train"
    else:
        mode = "test"
    # generate train/test dir
    if not os.path.exists(mode):
        os.makedirs(mode)
    #generate class dir under train/test
    for item in labels:
        class_dir = os.path.join(mode, item)
        if not os.path.exists(class_dir):
            os.makedirs(class_dir)    
            print(class_dir, "created!")
    assert len(os.listdir("train")) == len(labels), \
    f"Generated class dir under {mode} dir wrong"


def transform_2_img(numpy_data):
    # transform to a (3, h, w) array, but <numpy_data> is [1, 3072]
    SLOT = 32 * 32
    img = np.zeros((3, 32, 32), dtype=np.uint8) # for cifar10 and cifar100
    for i in range(3):
        channel = numpy_data[i * SLOT : (i + 1) * SLOT]
        channel = np.reshape(channel, (32, 32))
        img[i] = channel
    img = np.transpose(img, (1, 2, 0))
    final_img = Image.fromarray(np.uint8(img))
    return final_img


def make_for_cifar10(root):
    if not os.path.exists(root):
        raise FileExistsError(f"{root} does not exist!")
    
    filenames = sorted(os.listdir(root))
    if "batches.meta" in filenames:
        labels = unpickle("batches.meta")[b'label_names']
        labels = [i.decode("utf-8") for i in labels]
        print(f"Contains {len(labels)} labels")
    else:
        raise KeyError("batches.meta does not exists.")

    # generate train dir and class dir under it
    generate_class_dir(train=True, labels=labels)
    generate_class_dir(train=False, labels=labels)
    for item in filenames:
        if item.startswith("data_batch"):
            mode = "train"
        elif item.startswith("test_batch"):
            mode = "test"
        else:
            print(f"No processing on {item}")
        if item.startswith("1data_batch") or item.startswith("test_batch"):
            dict_data = unpickle(item)
            idx: str = dict_data[b'batch_label'].decode("utf-8")
            print(f"Processing {idx}...")
            numpy_data = dict_data[b'data']
            length, _ = numpy_data.shape
            for i, _ in tqdm(enumerate(numpy_data), total=length):
                img_name = dict_data[b'filenames'][i].decode("utf-8")
                img_label = dict_data[b'labels'][i]
                img = transform_2_img(numpy_data[i])
                real_class = labels[img_label]
                img_path = os.path.join(mode, real_class, img_name)
                img.save(img_path)
    
if __name__ == "__main__":
    main()
